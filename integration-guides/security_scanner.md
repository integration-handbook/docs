# 集成安全扫描器

将安全扫描器集成到 GitLab 是为终端用户提供 [CI Job 定义](https://docs.gitlab.com/ee/ci/yaml/index.html)，用户可以将其添加到他们的 CI 配置文件中以扫描他们的 GitLab 项目。然后，此 CI Job 需要以 GitLab 指定的格式输出其结果。然后这些结果会自动显示在 GitLab 的各个位置，例如 Pipeline 视图、Merge Request widget 和安全仪表板。

扫描作业通常是一个 [Docker 镜像](https://docs.docker.com/)，该镜像需要包含一个独立环境中的扫描器及其所有依赖项。

此页面记录了编写实现安全扫描器 CI Job 以及 Docker 镜像的要求和指南。

## Job 定义

本节描述了要添加到安全扫描器工作定义文件的几个重要字段。关于这些字段和其他可用字段的完整文档可以在 [CI 文档](https://docs.gitlab.com/ee/ci/yaml/index.html#image) 中查看。

### Name

为了保持一致性，扫描工作应以扫描器的名字命名需要使用小写。Job 后缀为扫描类型：
- `_dependency_scanning`
- `_cluster_image_scanning`
- `_container_scanning`
- `_dast`
- `_sast`

例如，基于 `MySec` 扫描器的依赖性扫描 Job 应命名为 `mysec_dependency_scanning`。

### Image

`image` 关键字用于指定包含安全扫描器的 [Docker 镜像](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-an-image)。

### Script

`script` 关键字用于指定运行扫描器的命令。因该项不能为空，它必须被设置为执行扫描的命令。不能依靠 Docker 镜像预定义的 `ENTRYPOINT` 和 `CMD` 来自动执行扫描，而不传递任何命令。

`before_script` 不应该在作业定义中使用，因为用户可能依靠它在执行扫描前准备他们的项目。例如，在执行SAST或依赖性扫描之前，使用 `before_script` 来安装特定项目所需的系统库是一种常见的做法。

同样地，`after_script` 也不应该在作业定义中使用，因为它可能被用户覆盖。

### Stage

在可能的情况下，扫描作业应该属于 `test` 阶段。stage 关键字可以省略，因为test 是默认值。

### Fail-safe

为了与 GitLab 安全范式保持一致，扫描 Job 在失败时不应该阻塞 Pipeline，所以` allow_failure` 参数应该设置为 true。

### Artifacts

扫描 Job 必须使用 `artifacts:reports` 关键字，声明一个与他们执行的扫描类型相对应的报告。有效的报告是：
- `dependency_scanning`
- `container_scanning`
- `cluster_image_scanning`
- `dast`
- `api_fuzzing`
- `coverage_fuzzing`
- `sast`

例如，这里是一个 SAST 作业的定义，它生成一个名为 `gl-sast-report.json` 的文件，并将其作为 SAST 报告上传。

```yaml
mysec_sast:
  image: registry.gitlab.com/secure/mysec
  artifacts:
    reports:
      sast: gl-sast-report.json
```

需要注意的是 `gl-sast-report.json` 只是一个文件路径的例子，但可以使用任何其他文件名。参见 [输出文件部分](https://docs.gitlab.com/ee/development/integrations/secure.html#output-file) 了解更多细节。它被处理为 SAST 报告是因为它在作业定义的 `reports:sast` 关键词下被声明，而不是因为文件名。

### Policies

某些 GitLab 工作流程，如 [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/customize.html#disable-jobs)，定义了 CI/CD 变量，以表明应该禁用特定的扫描。你可以通过寻找以下变量来检查这一点：
- `DEPENDENCY_SCANNING_DISABLED`
- `CONTAINER_SCANNING_DISABLED`
- `CLUSTER_IMAGE_SCANNING_DISABLED`
- `SAST_DISABLED`
- `DAST_DISABLED`

如果根据扫描器的类型合适，你应该禁用运行自定义扫描器。

GitLab 还定义了一个 `CI_PROJECT_REPOSITORY_LANGUAGES` 变量，它提供了仓库中的语言列表。根据这个值，你的扫描器可能会也可能不会做一些不同的事情。语言检测目前依赖于 `linguist`  Ruby gem，参见[预定义的 CI/CD 变量](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)。

#### Policy checking example

下面这个示例展示了如何跳过一个自定义的依赖性扫描作业，`mysec_dependency_scanning`，除非项目库包含 Java 源代码并且启用了依赖性扫描功能。

```yaml
mysec_dependency_scanning:
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $GITLAB_FEATURES =~ /\bdependency_scanning\b/
      exists:
        - '**/*.java'
```

任何额外的工作策略由用户根据需要进行配置。例如，预定义的策略不扫描特定的分支或当特定的文件集改变时触发扫描工作。

## Docker 镜像

Docker 镜像是一个独立的环境，它将扫描器及其依赖的所有库和工具结合起来。将扫描器打包成Docker 镜像，使其依赖性和配置始终存在，无论扫描器在哪里运行。

### 镜像大小

根据 CI 基础设施的不同，可能需要在每次作业运行时获取 Docker 镜像。为了使扫描工作快速运行并避免浪费带宽，Docker 镜像应尽可能小。最好是 50MB 或更小。如果这不可能，尽量保持在 1.46GB  以下，也就是一张 CD-ROM 的大小。

如果扫描器需要一个完整的 Linux 环境，建议使用 [Debian](https://www.debian.org/intro/about) 的 "slim" 发行版或 [Alpine Linux](https://www.alpinelinux.org/)。如果有可能的话，建议使用 `FROM scratch` 指令从零开始构建镜像，并使用所有需要的库来编译扫描器。多阶段的构建也有助于保持镜像的小型化。

使镜像尽可能的小，可以考虑使用 [dive](https://github.com/wagoodman/dive#dive) 来分析 Docker 镜像中的层，以确定多余的空间占用来自哪里。

在某些情况下，可能很难从镜像中删除文件。这时可以考虑使用 [Zstandard](https://github.com/facebook/zstd) 来压缩文件或大目录。Zstandard 提供了许多不同的压缩级别，在减少镜像的大小的同时对解压速度影响很小。当镜像启动时，自动解压压缩的目录。可以通过在 Docker 镜像的 `/etc/bashrc` 或特定用户的 `$HOME/.bashrc` 中添加一个步骤来实现。如果你选择了后者，记得修改 entry point 来启动一个 bash 登录的 shell。

可以参考如下示例：
- https://gitlab.com/gitlab-org/security-products/license-management/-/blob/0b976fcffe0a9b8e80587adb076bcdf279c9331c/config/install.sh#L168-170
- https://gitlab.com/gitlab-org/security-products/license-management/-/blob/0b976fcffe0a9b8e80587adb076bcdf279c9331c/config/.bashrc#L49

### 镜像 tag

正如 [Docker 官方镜像](https://github.com/docker-library/official-images#tags-and-aliases) 项目中所记录的那样，我们强烈建议给版本号标签加上别名，使得用户能够轻松地参考某个特定系列的"最新"版本。详见 [Docker 标签：对 Docker 镜像进行标记和版本管理的最佳实践](https://docs.microsoft.com/en-us/archive/blogs/stevelasker/docker-tagging-best-practices-for-tagging-and-versioning-docker-images)。

## 命令行

扫描器是一种 CLI 命令行工具，它以环境变量作为输入，并生成一个文件作为报告上传（基于作业定义）。它还在标准输出和标准错误流上生成文本输出，并以状态代码退出。

### 变量

所有 CI/CD 变量都作为环境变量传递给扫描器，扫描的项目由 [预定义的 CI/CD 变量](https://docs.gitlab.com/ee/ci/variables/index.html) 来描述。

#### SAST 和依赖扫描

SAST 和依赖扫描器必须扫描项目目录中的文件，该目录来自 `CI_PROJECT_DIR` CI/CD 变量。

#### 容器扫描

为了与 GitLab 官方容器扫描一致，扫描器必须扫描名称和标签分别为 `CI_APPLICATION_REPOSITORY` 和 `CI_APPLICATION_TAG` 给出的 Docker 镜像。如果提供了 `DOCKER_IMAGE` CI/CD 变量，那么 `CI_APPLICATION_REPOSITORY` 和 `CI_APPLICATION_TAG` 变量将被忽略，而扫描 `DOCKER_IMAGE` 变量中指定的图像。

如果没有提供，`CI_APPLICATION_REPOSITORY` 应默认为 `$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG`，它是预定义 CI/CD 变量的组合。`CI_APPLICATION_TAG` 应该默认为 `CI_COMMIT_SHA`。

扫描器应使用变量 `DOCKER_USER` 和 `DOCKER_PASSWORD` 登录 Docker registry。如果这些没有定义，那么扫描器应该使用 `CI_REGISTRY_USER` 和 `CI_REGISTRY_PASSWORD` 作为默认值。

#### 集群镜像扫描

为了与 GitLab 官方 `cluster_image_scanning` 一致，扫描器必须扫描 Kubernetes 集群，其配置由 `KUBECONFIG` 给出。

如果使用 `CIS_KUBECONFIG` CI/CD 变量，那么 `KUBECONFIG` 变量会被忽略，而会扫描 `CIS_KUBECONFIG` 变量中指定的集群。如果你没有提供 `CIS_KUBECONFIG` CI/CD 变量，那么该值默认为 `$KUBECONFIG` 的值。`$KUBECONFIG` 是在项目被分配到 Kubernetes 集群时配置的预定义 CI/CD 变量。当 `KUBECONFIG` 变量中提供了多个集群 contexts 时，将会选择 `current-context` 指定的集群。

#### 配置文件

虽然扫描器可以使用 `CI_PROJECT_DIR` 来加载特定的配置文件，但建议将配置暴露为 CI/CD 变量，而不是文件。

### 输出文件

像任何上传到 GitLab CI/CD 的制品一样，扫描器生成的安全报告必须保存在由 `CI_PROJECT_DIR` CI/CD 变量指定的项目目录下。

建议以扫描的类型来命名输出文件，并使用 `gl-` 作为前缀。由于所有安全报告都是 JSON 文件，建议使用 `.json` 作为文件扩展名。例如，依赖扫描报告的建议文件名是 `gl-dependency-scanning.json`。

Job 定义的 `artifacts:reports` 关键字必须与写入安全报告的文件路径一致。例如，如果依赖扫描分析器将其报告写入 CI 项目目录，报告文件名是 `depscan.json`，那么 `artifacts:reports:dependency_scanning` 必须被设置为 `depscan.json`。

### Exit code

按照 POSIX 的退出代码标准，扫描器以 0 退出表示成功，以 1 到 255 的任何数字退出表示其他情况。成功也包括发现漏洞的情况。

当使用 [Docker-in-Docker 特权模式](https://docs.gitlab.com/ee/user/application_security/sast/index.html#requirements) 执行扫描作业时，我们保留以下标准退出代码。

| Exit Code | 描述 |
| :------ |:--- |
| 3 | No match, no compatible analyzer |
| 4 | Project directory empty |
| 5 | No compatible Docker image |

### 日志

扫描器应该记录错误信息和警告，这样用户就可以通过查看 CI 扫描 Job 的日志轻松地调查错误配置和集成问题。

扫描器可以使用 [ANSI escape codes](https://en.wikipedia.org/wiki/ANSI_escape_code#Colors) 对 Unix 标准输出和标准错误流的信息着色。建议使用红色来报告错误，黄色代表警告，绿色代表通知。另外，我们建议用 `[ERRO]` 作为错误信息的前缀，用 `[WARN]` 作为警告，用 `[INFO]` 作为通知。

#### 日志级别

如果一个日志信息的级别低于 `SECURE_LOG_LEVEL` CI/CD 变量中设置的级别，扫描器应滤掉该信息。例如，当 `SECURE_LOG_LEVEL` 设置为 `error` 时，info 和 warn 信息应被跳过。可接受的值如下，从高到低排列:
- `fatal`
- `error`
- `warn`
- `info`
- `debug`

在调试时建议使用 `debug` 级别，这样可以获得更多详细的日志。`SECURE_LOG_LEVEL` 的默认值应该被设置为 `info`。

当执行命令行时，扫描器应该使用 `debug` 级别来记录命令行和它的输出。例如，[bundler-audit](https://gitlab.com/gitlab-org/security-products/analyzers/bundler-audit) 扫描器使用 `debug` 级别来记录命令行 `bundle audit check --quiet`，以及 `bundle audit` 写入标准输出的内容。如果命令行失败，则应使用 `error` 日志级别进行记录；这使得调试问题成为可能，而无需将日志级别更改为 `debug` 并重新运行扫描作业。

#### 常见的 `logutil` 包

如果你使用 [go](https://golang.org/) 和 [common](https://gitlab.com/gitlab-org/security-products/analyzers/common)，那么建议你使用 [Logrus](https://github.com/Sirupsen/logrus) 和 [common 的 logutil 包](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/tree/master/logutil) 来配置 Logrus 的格式化。参见 [`logutil` README](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/tree/master/logutil/README.md)

## 报告

该报告是一个 `JSON` 文档，将漏洞与可能的补救措施结合起来。

本文档概述了报告 JSON 格式，以及帮助集成商设置其字段的建议和示例。该格式在 [SAST](https://docs.gitlab.com/ee/user/application_security/sast/index.html#reports-json-format)、[DAST](https://docs.gitlab.com/ee/user/application_security/dast/#reports)、[Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html#reports-json-format)、[Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html#reports-json-format) 和 [Cluster Image Scanning](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/index.html#reports-json-format) 的文档中进行了广泛的描述。

你可以在下面找到这些扫描器的模式：
- [SAST](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/sast-report-format.json)
- [DAST](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/dast-report-format.json)
- [Dependency Scanning](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/dependency-scanning-report-format.json)
- [Container Scanning](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/container-scanning-report-format.json)
- [Coverage Fuzzing](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/coverage-fuzzing-report-format.json)
- [Secret Detection](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/secret-detection-report-format.json)

### Version

这个字段指定了你正在使用的 [安全报告模式](https://gitlab.com/gitlab-org/security-products/security-report-schemas) 的版本。请参考模式的[发布](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/releases)，以了解要使用的具体版本。

### 漏洞

报告中的 `vulnerabilities` 字段是一个漏洞对象的数组。

#### ID

`id` 字段是该漏洞的唯一标识符。它用于从 [修复对象](https://docs.gitlab.com/ee/development/integrations/secure.html#remediations) 中引用一个固定的漏洞。我们建议你生成一个 UUID 并将其作为 `id` 字段的值。

#### Category

该 `category` 字段的值与报告类型相匹配：
- dependency_scanning
- cluster_image_scanning
- container_scanning
- sast
- dast

#### Scanner

`scanner` 字段是一个嵌入了可读 `name` 和技术 `id` 的对象。该 `id` 不应该与其他集成商提供的任何其他扫描仪相冲突。

#### Name, message, and description

`name` 和 `message` 字段包含该漏洞的简短说明，`description` 字段提供了更多详细信息。

`name` 字段与上下文无关，不包含有关发现漏洞 `message` 的位置的信息，而可能重复该位置。

作为可视化示例，下面截图突出显示了在将漏洞作为 Pipeline 视图的一部分查看时使用这些字段的位置。

![](/static/example_vuln.png)

例如，`message` 依赖扫描报告的漏洞的 a 提供了有关易受攻击的依赖项的信息，这与 `location` 漏洞的字段是多余的。该 `name` 字段是首选 `message` 字段，但当 context/location 无法从漏洞标题中删除时使用该字段。

为了说明，这里是一个由依赖扫描器报告的示例漏洞对象，其中 `message` 重写了 `location` 字段：
```json
{
    "location": {
        "dependency": {
            "package": {
            "name": "debug"
          }
        }
    },
    "name": "Regular Expression Denial of Service",
    "message": "Regular Expression Denial of Service in debug",
    "description": "The debug module is vulnerable to regular expression denial of service
        when untrusted user input is passed into the `o` formatter.
        It takes around 50k characters to block for 2 seconds making this a low severity issue."
}
```

`description` 字段解释该漏洞是如何工作的，或给上下文有关该漏洞。不应重复漏洞对象的其他字段。特别是，`description` 不应与 `location`（受影响的内容）或 `solution`（如何减轻风险）重复。

#### Solution

您可以使用 `solution` 字段来指导用户如何修复已识别的漏洞或降低风险。终端用户与此字段交互，而 GitLab 会自动处理 `remediations` 对象

#### Identifiers

`identifiers` 数组描述了检测到的漏洞。标识符对象的 `type` 和 `value` 字段用于判断两个标识符是否相同。用户界面使用对象的 `name` 和 `url` 字段来显示标识符。

建议重用 GitLab 扫描器已经定义的标识符：

| Identifier | Type | Example value |
| :------ |:--- | :--- |
| [CVE](https://cve.mitre.org/cve/) | `cve` | CVE-2019-10086 |
| [CWE](https://cwe.mitre.org/data/index.html) |	cwe |	CWE-1026 |
| [OSVD](https://cve.mitre.org/data/refs/refmap/source-OSVDB.html) | osvdb |	OSVDB-113928 |
| [USN](https://ubuntu.com/security/notices)	| usn | USN-4234-1 |
| [WASC](http://projects.webappsec.org/Threat-Classification-Reference-Grid) | wasc | WASC-19 |
| [RHSA](https://access.redhat.com/errata/#/) | rhsa | RHSA-2020:0111 |
| [ELSA](https://linux.oracle.com/security/) | elsa | ELSA-2020-0085 |

上面列出的通用标识符在 [公共库](https://gitlab.com/gitlab-org/security-products/analyzers/common) 中定义，它由 GitLab 维护的一些分析器共享。如果需要，您可以 [提供](https://gitlab.com/gitlab-org/security-products/analyzers/common/blob/master/issue/identifier.go) 新的通用标识符。分析器还可以生成不属于 [公共库](https://gitlab.com/gitlab-org/security-products/analyzers/common) 的特定于供应商或特定于产品的标识符。

`identifiers` 数组的第一项称为 [主标识符](https://docs.gitlab.com/ee/user/application_security/terminology/#primary-identifier)。主要标识符特别重要，因为它用于在 新提交被推送到存储库时 [跟踪漏洞](https://docs.gitlab.com/ee/user/application_security/terminology/#primary-identifier)。标识符还用于 [合并](https://docs.gitlab.com/ee/development/integrations/secure.html#tracking-and-merging-vulnerabilities) 为同一提交报告的 [重复漏洞](https://docs.gitlab.com/ee/development/integrations/secure.html#tracking-and-merging-vulnerabilities)，除了 `CWE` 和 `WASC`。

并非所有漏洞都具有 CVE，并且可以多次识别 CVE。因此，CVE 不是一个稳定的标识符，您在跟踪漏洞时不应假设它是稳定的。

漏洞的最大标识符数设置为 20 个。如果漏洞的标识符超过 20 个，只保存前 20 个。请注意，[Pipeline Security](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#pipeline-security) 选项卡中的漏洞不会强制执行此限制，并且会显示报告工件中存在的所有标识符。

### Location

`location` 表示检测到漏洞的位置。location 的格式取决于扫描的类型。

GitLab 在内部提取的一些属性 `location` 以生成**位置指纹**，用于在新提交被推送到存储库时跟踪漏洞。用于生成位置指纹的属性也取决于扫描类型。

### Details

该 `details` 字段是一个对象，支持查看漏洞信息时显示的许多不同的内容元素。可以在 [security-reports repository](https://gitlab.com/gitlab-examples/security/security-reports/-/tree/master/samples/details-example) 中看到各种数据元素的示例。

#### Dependency Scanning

在 `location` 一个依赖扫描漏洞是由的 `dependency` 和 `file`。该 `dependency` 对象描述了受影响的 `package` 和依赖关系 `version`。`package` 嵌入 `name` 受影响的库/模块。 `file` 是声明受影响依赖项的依赖项文件的路径。

例如，这是 `location` 影响 `4.0.11` npm 包版本的漏洞的对象 [handlebars](https://www.npmjs.com/package/handlebars)：
```json
{
    "file": "client/package.json",
    "dependency": {
        "package": {
            "name": "handlebars"
        },
        "version": "4.0.11"
    }
}
```
这个受影响的依赖项在 `client/package.json` npm 或 yarn 处理的依赖项文件中列出。

依赖扫描漏洞的位置指纹结合了`file` 和包 `name`，因此这些属性是必需的。所有其他属性都是可选的。

#### Container Scanning

类似于依赖扫描，`location` 容器扫描漏洞的有一个 `dependency` 和 `file`。它也有一个 `operating_system` 领域。

例如，这是 `location` 影响 `2.50.3-2+deb9u1` Debian 软件包版本的漏洞的对象 `glib2.0`：
```json
{
    "dependency": {
        "package": {
            "name": "glib2.0"
        },
    },
    "version": "2.50.3-2+deb9u1",
    "operating_system": "debian:9",
    "image": "registry.gitlab.com/example/app:latest"
}
```
扫描 Docker 镜像时发现受影响的包 `registry.gitlab.com/example/app:latest`。Docker 映像基于 `debian:9`(Debian Stretch)。

容器扫描漏洞的位置指纹结合了 `operating_system` 和包 `name`，因此这些属性是必需的。该 `image` 也是强制性的。所有其他属性都是可选的。

#### Cluster Image Scanning

在 `location` 一个的 `cluster_image_scanning` 漏洞，有一个 `dependency` 领域。它也有一个 `operating_system` 领域。例如，这是 `location` 影响 `2.50.3-2+deb9u1` Debian 软件包版本的漏洞的对象 `glib2.0`：
```json
{
    "dependency": {
        "package": {
            "name": "glib2.0"
        },
    },
    "version": "2.50.3-2+deb9u1",
    "operating_system": "debian:9",
    "image": "index.docker.io/library/nginx:1.18"
}
```
扫描 pod 镜像时发现受影响的包 `index.docker.io/library/nginx:1.18`。

集群图像扫描漏洞的位置指纹结合了 `operating_system` 和包 `name`，因此这些属性是必需的。该 `image` 也是强制性的。所有其他属性都是可选的。

#### SAST

在 `location` 一个 SAST 脆弱性必须有一个 `file` 和 `start_line` 领域，使受影响的文件的路径，以及受影响的行数，分别。它也可能有一个 `end_line`、一个 `class`和一个 `method`。

举例来说，这里是location在系中发现一个安全漏洞对象41的src/main/java/com/gitlab/example/App.java，在generateSecretToken该方法com.gitlab.security_products.tests.App的Java类：
```json
{
    "file": "src/main/java/com/gitlab/example/App.java",
    "start_line": 41,
    "end_line": 41,
    "class": "com.gitlab.security_products.tests.App",
    "method": "generateSecretToken1"
}
```
一个 SAST 漏洞结合了位置的指纹 `file``start_line` 和 `end_line`，因此这些属性是强制性的。所有其他属性都是可选的。

### 跟踪和合并漏洞

用户可以对漏洞进行反馈：
- 如果漏洞不适用于他们的项目，他们可能会忽略
- 如果存在可能的威胁，他们可能会为漏洞创建问题

GitLab 跟踪漏洞，以便在将新的 Git 提交推送到存储库时不会丢失用户反馈。使用三个属性的组合来跟踪漏洞：
- [报告类型](https://docs.gitlab.com/ee/development/integrations/secure.html#category)
- [位置指纹](https://docs.gitlab.com/ee/development/integrations/secure.html#location)
- [主要标识符](https://docs.gitlab.com/ee/development/integrations/secure.html#identifiers)

现在，如果在推送新的 Git 提交时其位置发生变化，GitLab 无法跟踪漏洞，这会导致用户反馈丢失。例如，如果受影响的文件被重命名或受影响的行向下移动，则用户对 SAST 漏洞的反馈将丢失。这在 [Issue #7586](https://gitlab.com/gitlab-org/gitlab/-/issues/7586) 中得到解决。

在某些情况下，在同一 CI 管道中执行的多次扫描会导致使用漏洞位置和标识符自动合并的重复项。如果两个漏洞共享相同的 [位置指纹](https://docs.gitlab.com/ee/development/integrations/secure.html#location) 和至少一个 [标识符](https://docs.gitlab.com/ee/development/integrations/secure.html#identifiers)，则认为它们是相同的。如果两个标识符共享相同的 `type`，则它们是相同的 `id`。不考虑 CWE 和 WASC 标识符，因为它们描述了漏洞缺陷的类别，而不是特定的安全缺陷。

#### Severity and confidence

`severity` 字段描述了漏洞对软件的影响程度，而 `confidence` 字段描述了漏洞评估的可靠性。严重性用于对安全仪表板中的漏洞进行排序。

严重程度从 `Info` 到 `Critical`，但也可以是 `Unknown`。有效值是：`Unknown`，`Info`，`Low`，`Medium`，`High`，或者 `Critical`

置信度范围从 `Low` 到 `Confirmed`，但也可以是 `Unknown`，`Experimental` 甚至 `Ignore` 可以忽略漏洞。有效值是：`Ignore`，`Unknown`，`Experimental`，`Low`，`Medium`，`High`，或者 `Confirmed`

`Unknown` 值意味着数据无法确定其实际值。因此，它可能是 `high`、`medium` 或 `low`，需要进行调查。我们提供了可用 SAST 分析器的 [chart](https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html#analyzers-data) 以及当前可用的数据。

### Remediations

报告的 `remediations` 字段是一个补救措施对象的数组。每个补救措施都描述了一个可用于 [解决](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#resolve-a-vulnerability) 一组漏洞的补丁。

下面是一个包含补救措施的报告的例子：
```json
{
    "vulnerabilities": [
        {
            "category": "dependency_scanning",
            "name": "Regular Expression Denial of Service",
            "id": "123e4567-e89b-12d3-a456-426655440000",
            "solution": "Upgrade to new versions.",
            "scanner": {
                "id": "gemnasium",
                "name": "Gemnasium"
            },
            "identifiers": [
                {
                  "type": "gemnasium",
                  "name": "Gemnasium-642735a5-1425-428d-8d4e-3c854885a3c9",
                  "value": "642735a5-1425-428d-8d4e-3c854885a3c9"
                }
            ]
        }
    ],
    "remediations": [
        {
            "fixes": [
                {
                    "id": "123e4567-e89b-12d3-a456-426655440000"
                }
            ],
            "summary": "Upgrade to new version",
            "diff": "ZGlmZiAtLWdpdCBhL3lhcm4ubG9jayBiL3lhcm4ubG9jawppbmRleCAwZWNjOTJmLi43ZmE0NTU0IDEwMDY0NAotLS0gYS95Y=="
        }
    ]
}
```

#### Summary

`summary` 字段概述了如何修复漏洞。此字段是必需的。

#### Fixed vulnerabilities

`fixes` 字段是引用修复修复的漏洞的对象数组。`fixes[].id` 包含固定漏洞的 [唯一标识符](https://docs.gitlab.com/ee/development/integrations/secure.html#id)。此字段是必需的。

#### Diff

`diff` 字段是 base64 编码的修复代码差异，与 [git apply](https://git-scm.com/docs/git-format-patch#_discussion) 兼容. 此字段是必需的。

## 限制

### Container Scanning

容器扫描目前有以下限制：
- 虽然安全仪表板可以显示来自多个镜像的扫描结果，但如果多个漏洞具有相同的指纹，则只会显示该漏洞的第一个实例。我们正在努力消除此限制。您可以关注我们在 [更改容器扫描的位置指纹](https://gitlab.com/gitlab-org/gitlab/-/issues/215466) 问题上的进展 。
- 不同的扫描程序可能各自报告相同的漏洞，从而导致重复的发现。
