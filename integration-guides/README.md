## 贡献指南

本指南准备了一些参与贡献前需要了解的信息，阅读这些信息能帮助您更快的了解到如何参与贡献。

- 使用 [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/README.md) 配置 GitLab 开发环境
- [GitLab 贡献指南](https://docs.gitlab.com/ee/development/contributing/index.html)
    - 从 [Issue 工作流](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html)中获取有关以下内容的更多信息：
        - Issue 跟踪器指南
        - Issue 分类
        - Labels
        - 功能建议
        - Issue 权重
        - Issue 回顾
        - 技术债
    - [MR 工作流](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html)以获取更多信息：
        - Merge request 指南
        - 贡献的接受标准
        - 任务完成的定义
        - 依赖性
    - [贡献风格指南](https://docs.gitlab.com/ee/development/contributing/style_guides.html)
    - [实现设计和 UI 元素](https://docs.gitlab.com/ee/development/contributing/design.html)
- [GitLab 架构概述](https://docs.gitlab.com/ee/development/architecture.html)
- [Rake tasks](https://docs.gitlab.com/ee/development/rake_tasks.html) for development

更多内容，见 [Contributor and Development Docs](https://docs.gitlab.com/ee/development/index.html#integration-guides)
