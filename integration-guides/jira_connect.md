# 集成 Jira 开发环境搭建

搭建 Jira 与 GItLab 集成开发环境需要以下内容：
- 一个 Jira Cloud 实例。Atlassian [提供免费的开发和测试实例](https://developer.atlassian.com/platform/marketplace/getting-started/#free-developer-instances-to-build-and-test-your-app)。
- 可通过 Internet 访问的 GitLab 实例。为了让应用程序正常工作，请确保 Jira Cloud 能够通过互联网连接到 GitLab 实例。为此，我们建议使用 Gitpod 或类似的云开发环境。有关在 GDK 中使用 Gitpod 的更多信息，请参阅：
    - [GDK in Gitpod](https://www.loom.com/share/9c9711d4876a40869b9294eecb24c54d) 视频
    - [Gitpod with GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/gitpod.md) 文档

**请不要使用隧道工具**，例如 Serveo 或 `ngrok`. 这些都存在安全风险，禁止在开发人员的电脑上运行。

Jira 要求与应用程序主机的所有连接都通过 SSL。所以在配置自己的环境时，请启用 SSL 和使用适当的证书。

## 在 Jira 上安装应用

在 Jira 中安装应用程序：
1. 启用 Jira 开发模式以安装不是来自 Atlassian Marketplace 的应用程序：
    1. 在 Jira 中，导航到 **设置** > **应用程序** > **管理应用**。
    1. 滚动到**管理应用程序**页面的底部，然后点击**设置**。
    1. 选择启用 `Enable development mode`。
1. 安装应用：
    1. 在 Jira 中，导航到 **设置** > **应用程序** > **管理应用**。
    1. 点击**上传应用**。
    1. 输入 app descriptor 的 URL 来上传应用并安装。主机和端口必须指向您的 GitLab 实例。
        例如：
        ```shell
        https://xxxx.gitpod.io/-/jira_connect/app_descriptor.json
        ```
    1. 点击**上传**。

如果安装成功，您应该会在 **Manage apps** 下看到 **GitLab for Jira** 应用。您还可以单击 **Getting Started** 以打开您的 GitLab 实例的配置页面。

> *请注意，对 app descriptor 的任何更改都需要您卸载然后重新安装该应用程序。*

### 故障排查

如果应用安装失败，您可能需要从数据库中删除 `jira_connect_installations` 表。

1. 打开[数据库控制台](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/postgresql.md#access-postgresql)。
1. 运行 `TRUNCATE TABLE jira_connect_installations CASCADE;`。


## 添加 namespace

使用 Jira 添加 GItLab namespace：

1. 确保您已登录开发 GitLab 实例。
1. 在 Jira 中的 GitLab 应用程序页面上，单击 **Get started**。
1. `F12` 打开浏览器的开发者工具并导航到 **Network** 选项卡。
1. 尝试在 Jira 中添加 namespace。
1. 如果请求失败并显示 `401 “not authorized”`，请将请求复制为 cURL 命令并将其粘贴到您的终端中。  
  ![Example Vulnerability](/static/copy_curl.png)
1. 转到您的开发实例（ 通常位于：http://localhost:3000 ），打开开发者工具，导航到 Network 选项卡并刷新页面。
1. 复制第一个请求中的所有 cookie。  
  ![Example Vulnerability](/static/copy_cookies.png)
1. 将 cookie 附加到终端中的 cURL 命令： `--cookies "<cookies from the request>"`。
1. 发送 cURL 请求。
1. 如果响应是 `{"success":true}`，则 namespace 添加成功。

