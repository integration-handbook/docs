# 安全产品集成流程

本章节提供了合作伙伴集成所需的相关资源，以下步骤是完成集成所需步骤和相关操作详细说明的链接。

## 集成层

GitLab 中的安全产品是为 GitLab 旗舰版用户和 [DevSecOps](https://about.gitlab.cn/solutions/dev-sec-ops/) 用户设计的。所有功能都在该层中，包括为用户提供一致体验所需的 API 和标准报告框架，以便将用户喜欢的安全工具轻松引入 GitLab。我们要求集成合作伙伴将他们的工作集中在这些许可层上，以便我们能为我们的共同客户提供最大的价值。


## GitLab Developer Workflow

此工作流是 GitLab 建议用户与 GitLab 交互的方式。了解用户如何使用 GitLab 可帮助您选择将产品及其结果集成到 GitLab 的最佳位置。

- 开发者希望在不打开新工具的情况下，通过编写代码完成任务或解决问题。
- 开发者提交代码到 Git 分支并创建 MR，该 MR 可以被 Review 并且会触发 GitLab Pipeline 来运行相关任务，其中就包含代码安全检查。
- Pipeline Job 有多种用途，可以进行应用安全、公司政策或合规性扫描。当 Job 运行完成后，会报告状态，并将生成的报告保存在 [Job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) 中。
- [MR 安全组件](https://docs.gitlab.com/ee/user/project/merge_requests/testing_and_reports_in_merge_requests.html#security-reports) 会展示 Pipeline 的安全检查结果，开发者可以在 MR 页面 Review 扫描结果的摘要和详情。
- 如果一个项目制定了某些策略（如 [merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/index.html)），则开发人员必须完成指定的扫描且正常通过或取得指定人员的批准。
- 在 [安全仪表盘](https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html) 中也会展示相关结果，开发者可以通过安全仪表盘快速检索代码中所有需要解决的漏洞。
- 当开发者阅读有关漏洞的细节时，会得到更多相关信息并进行下一步操作：
     - 创建 Issue（确认发现）：创建一个要优先处理的新 Issue。
     - 添加评论和修改状态：当一个发现漏洞时，用户可以发表评论并修改漏洞状态，指出他们已经确认并将修复该漏洞，或忽略该漏洞。
     - 自动修复/创建 MR：自动修复漏洞，尽可能提供一种不需要用户消耗额外精力，一键修复漏洞的简单解决方案。
     - 链接：展示外部来源链接，让用户获得该漏洞的更多数据

## 如何加入

介绍合作伙伴如何加入并完成安全产品集成所需完成的步骤。

1. 阅读 [集成手册 - 概述篇](/integration-guides/README.md)
1. 使用新合作伙伴模板创建 Issue 并进行讨论
1. 申请测试账号
1. 提供一个 [Pipeline](https://docs.gitlab.com/ee/development/pipelines.html) CI Template，用户将使用该模板在自己的 CI 中运行安全扫描
1. CI Pipeline 生成 report artifact
1. 确保您的 CI Pipeline 会生成报告并将报告保存在 Job artifacts 中，GitLab 会解析该 report artifact，以便在 GitLab 的其他部分展示结果
     - 参考[集成安全扫描器](security_scanner.md)
     - [job report artifacts](https://docs.gitlab.com/ee/ci/yaml/index.html#artifactsreports) 参考
     - [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) 参考
     - 您的 report artifacts 必须采用我们支持的格式之一，更多内容见 [文档](https://docs.gitlab.com/ee/development/integrations/secure.html#report)
        - [SAST 报告](https://docs.gitlab.com/ee/user/application_security/sast/index.html#reports-json-format) 文档
        - [依赖扫描报告](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html#reports-json-format) 文档
        - [容器扫描报告](https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html#reports-json-format) 文档
        - [集群镜像扫描报告](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/index.html#reports-json-format) 文档
        - [CI Template 示例](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml)
     - 一旦 Job 完成，数据会在如下几处展示：
        - 在 [Merge Request Security Report](https://docs.gitlab.com/ee/user/project/merge_requests/testing_and_reports_in_merge_requests.html#security-reports) ([MR Security Report data flow](https://gitlab.com/snippets/1910005#merge-request-view)).
        - 浏览 [Job Artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)
        - 在 [安全仪表盘](https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html) ([Dashboard data flow](https://gitlab.com/snippets/1910005#project-and-group-dashboards))
1. （可选）提供一种与作为漏洞结果页交互的方式：
     - 用户可以在他们的工作流程中与您的 artifact 的结果进行交互。他们可以忽略调查结果或接受它们并创建 backlog Issue
     - 要在没有用户交互的情况下自动创建问题，请使用 [Issue API](https://docs.gitlab.com/ee/api/issues.html)
1. （可选）提供自动修复：
     - 如果您在 artifact 中指定了 `remediations`，则建议通过我们的 [修复](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#resolve-a-vulnerability) 接口
1. 集成 Demo：
    - 当完成测试并准备好 Demo时，请联系我们，否则将无法得到 Marketing 的支持
1. 极狐 GitLab Marketing 支持
