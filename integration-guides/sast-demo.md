# SAST 安全扫描示例

所有安全扫描的 CI Template 都保存在 gitlab 项目仓库中，在使用时，用户只需要在 `.gitlab-ci.yml` 中 `include` 相应 Template 即可，比如 SAST 的模板就是 `Security/SAST.gitlab-ci.yml`。该模板 [源文件](https://gitlab.com/gitlab-jh/gitlab/-/blob/main-jh/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml) 保存在 gitlab 仓库中，合作伙伴在集成安全扫描器时，可以用来参考。

本文以 Flask 源码为例，展示安全扫描的用法和展示效果。

## Flask SAST Demo

首先 Fork [Flask 源码](https://github.com/pallets/flask) 到 gitLab.cn。

### 编写 `.gitlab-ci.yml`

之后点击 `CI/CD` -> `编辑器` -> `创建新的 Ci/CD 流水线` 为项目创建 `.gitlab-ci.yml`。当然也可以使用 `WebIDE` 或 `新建文件` 来添加，这里推荐使用流水线编辑器的原因是，流水线编辑器会实时监测 `.gitlab-ci.yml` 的语法并展示渲染效果，非常好用。

正常使用官方默认 SAST 扫描器非常简单，只需导入 CI Template 即可：

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SAST_EXPERIMENTAL_FEATURES: "true"
```

我们为了方便展示 `.gitlab-ci.yml` 的写法，不使用 CI Template，采取正常写法，将 Flask 可以用到的几个扫描器都写出来，内容如下：

```yaml
variables:
  # Setting this variable will affect all Security templates
  # (SAST, Dependency Scanning, ...)
  SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/gitlab-org/security-products/analyzers"

  SAST_EXCLUDED_ANALYZERS: ""
  SAST_EXCLUDED_PATHS: "spec, test, tests, tmp"
  SCAN_KUBERNETES_MANIFESTS: "false"

  SAST_EXPERIMENTAL_FEATURES: "true"

sast:
  stage: test
  artifacts:
    reports:
      sast: gl-sast-report.json
  rules:
    - when: never
  variables:
    SEARCH_MAX_DEPTH: 4
  script:
    - echo "$CI_JOB_NAME is used for configuration only, and its script should not be executed"
    - exit 1

.sast-analyzer:
  extends: sast
  allow_failure: true
  # `rules` must be overridden explicitly by each child job
  # see https://gitlab.com/gitlab-org/gitlab/-/issues/218444
  script:
    - /analyzer run

bandit-sast:
  extends: .sast-analyzer
  image:
    name: "$SAST_ANALYZER_IMAGE"
  variables:
    SAST_ANALYZER_IMAGE_TAG: 2
    SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/bandit:$SAST_ANALYZER_IMAGE_TAG"
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $SAST_EXCLUDED_ANALYZERS =~ /bandit/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.py'

eslint-sast:
  extends: .sast-analyzer
  image:
    name: "$SAST_ANALYZER_IMAGE"
  variables:
    SAST_ANALYZER_IMAGE_TAG: 2
    SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/eslint:$SAST_ANALYZER_IMAGE_TAG"
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $SAST_EXCLUDED_ANALYZERS =~ /eslint/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.html'
        - '**/*.js'
        - '**/*.jsx'
        - '**/*.ts'
        - '**/*.tsx'

semgrep-sast:
  extends: .sast-analyzer
  image:
    name: "$SAST_ANALYZER_IMAGE"
  variables:
    SAST_ANALYZER_IMAGE_TAG: 2
    SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/semgrep:$SAST_ANALYZER_IMAGE_TAG"
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $SAST_EXCLUDED_ANALYZERS =~ /semgrep/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - '**/*.py'
        - '**/*.js'
        - '**/*.jsx'
        - '**/*.ts'
        - '**/*.tsx'
        - '**/*.c'
```

这里用到了三个扫描器：[Bandit](https://gitlab.com/gitlab-org/security-products/analyzers/bandit)、[ESLint](https://gitlab.com/gitlab-org/security-products/analyzers/eslint)、[Semgrep](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep)，点击链接可以看到 GitLab 是如何将这些工具封装成符合 GitLab 要求的应用并进行打包的。

### 扫描效果

CI/CD  Pipeline 运行完成后，在 **安全与合规** -> **漏洞报告** 就可以查看扫描结果了：

![](/static/sast-flask.png)

点击单条漏洞，还可以查看该漏洞详细，详情中可以看到漏洞等级、具体位置和相关链接等信息，还可以快速为该漏洞创建 Issue，方便后续跟踪：

![](/static/sast-flask-detail.png)

### 更多

完整示例代码：https://gitlab.cn/cloud-native/gitlab/integration/flask-security-scanner-demo
