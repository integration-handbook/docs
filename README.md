![](/static/title.png)

极狐(GitLab) 是一款开源的一体化 DevOps 平台，以自信的速度、绝对的安全和可见性，成功实现DevOps 研运效能最大化，助力DevOps成功。同时极狐(GitLab) 秉承开源开放的策略，不仅可以作为一体化 DevOps 平台，还可以通过与其他应用软件的集成，在不给用户带来更多负担的情况下，为用户提供更多更优质的 DevOps 能力。

本手册旨在给希望与 GitLab 集成的合作伙伴提供一般性指导，同时也为了让更多用户了解目前 GitLab 与第三方应用的集成现状，以及如何集成和使用这些应用。

## 集成类型

访问 [GitLab 技术合作伙伴社区](https://about.gitlab.com/partners/technology-partners/)，可以看到目前所有与 GitLab 进行集成的应用，集成类型见下图，包括：Cloud、Kubernetes、IOS/Android Apps、CLI Clients 等。

![GitLab Technology Partners](https://tva3.sinaimg.cn/large/ad5fbf65gy1grbzzsn0ovj21lu0ru78e.jpg)

## 集成方式

GitLab 提供了多种集成方式，其中最主要的就是**与 GitLab 直接集成**和**使用 GitLab API**。

### 贡献代码

这也是 GitLab 推荐的一种方式，直接相应功能集成在 GitLab 中，需要使用的用户，只需要在 `Settings（设置） -> Integrations（集成）` 中开启相应功能即可，相关代码都可以在 [GitLab 代码库](https://gitlab.com/gitlab-org/gitlab)中找到。

**Tips**：想要找到相应集成应用的代码，可以从 Issue 或 MR 中通过 Label 搜索，如与集成 Jira 相关的代码，可以通过搜索包含 `Integration::Jira` 的 Issue 来找到相关代码和其都做了哪些集成的内容。

![Issue Page](https://tvax3.sinaimg.cn/large/ad5fbf65gy1grc130083qj211u0ikn0b.jpg)

如果要在本地搭建 GitLab 的开发环境，可以使用 GitLab 提供的开发者套件 [GDK（GitLab Development Kit）](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/README.md) 快速搭建一套 GitLab 开发环境。

### API

除了集成代码以外，如果您是要将 GitLab 集成到自己的应用中，请使用 GitLab API。GitLab 提供了三种集成方式：

- [GitLab REST API](https://docs.gitlab.com/ee/api/README.html) - 使用 GitLab 的 REST API 进行集成
- [GitLab GraphQL API](https://docs.gitlab.com/ee/api/graphql/index.html) - 使用 GitLab 的 GraphQL API 进行集成
- [第三方集成](https://docs.gitlab.com/ee/integration/index.html) - 就是集成已经与 GitLab 集成的产品

GitLab 的 [technology partners](https://about.gitlab.com/partners/technology-partners/#api-clients) 中还有一些提供多种语言的 API Client（也可称作 SDK），他们对 GitLab API 进行了封装，方便在各种语言下使用，如果不想自己处理安全认证和 API 封装，可以直接使用这些 API Client。

### Webhook

GitLab 中的某些事件会触发 Webhook，如代码 push 成功，CI Pipeline 构建成功等。当发生这些事件时，GitLab 可以向配置 Webhook 的 URL 发出 HTTP 请求，之后处理请求的逻辑由接收 HTTP 请求的应用完成。比较常见的用途就是在持续集成时追踪构建进度以及在错误发生时发出通知。

一般情况下，Webhook 在追踪 Issue 状态、触发 CI Job，以及一些自动化场景中有较多的使用，更多内容见 [官方文档](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)。

### CI Template

使用 CI Template 与 GitLab 进行集成的大多为安全扫描器，这样可以方便用户在 CI Pipeline 的时候进行安全扫描，从而达到 DevSecOps 的目的。提供 CI Template 则方便了 `.gitlab-ci.yml` 的配置，同时 CI job 需要以指定格式输出其结果，这样输出的结果就可以自动展示在 GitLab 的各种地方，如：Pipeline 页面、MR 组件和安全仪表盘，更多要求详见[官方文档](https://docs.gitlab.com/ee/development/integrations/secure.html)。

除了安全扫描之外，CI Template 还可以应用在多个领域，用来简化 GitLab CI 的流程、降低使用难度、完成特定的任务等，更多内容见 [CI/CD Template 开发者手册](https://docs.gitlab.com/ee/development/cicd/templates.html#development-guide-for-gitlab-cicd-templates)。

## 成为技术合作伙伴

极狐 GitLab 非常欢迎技术合作伙伴，并愿意为合作伙伴提供各种支持，通过产品集成的方式，为用户提供优质的 DevOps 体验。

目前我们支持通过我们的 API 和直接将功能集成在极狐 GItLab 中的方式，鼓励合作伙伴与我们共同维护集成。

<!-- ### 极狐 GitLab

我们非常欢迎合作伙伴的集成，可以进入官网，注册并创建 Issue，可与极狐的同学取得联系，我们会提供相应的帮助。

### GitLab Inc.

访问 GitLab Inc.(gitlab.com) 官网，进入 [GitLab Partner Program](https://partners.gitlab.com/English/) 注册为合作伙伴，其中包括以下两步：

#### Step 1：在官站注册并创建 Issue

1. 在官网完成[注册](https://partners.gitlab.com/) - 请确保在 Partner Applicant Type 下选择 `Technology/Software/Platform`，然后在Partner-Type 选择 `Technology Integration`。
2. 使用 [“New Partner” template](https://gitlab.com/gitlab-com/alliances/alliances/issues/new?issuable_template=new_partner) 填写 Issue。
3. 如果你是一个想要集成到我们的 **安全**或**保护** 阶段的合作伙伴，请访问安全合作伙伴集成[流程页面](https://docs.gitlab.com/ee/development/integrations/secure_partner_integration.html)了解更多信息。
4. 如果是与 GitLab 其他阶段进行集成，可以通过通过 [API](https://docs.gitlab.com/ee/api/)、[Webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) 或 [CI Template](https://docs.gitlab.com/ee/development/cicd/templates.html#development-guide-for-gitlab-cicd-templates) 完成集成。
5. 如果关于集成还有其他问题，可以参加每两周一次的 [Technology Partner Office Hours](https://calendar.google.com/calendar/selfsched?sstoken=UUtGOTlrbVNIbHVjfGRlZmF1bHR8MzU5ZWY1MzY2NzAxNmU5YmYxODZlYWM3YWU5ODZjNzQ)，会议时间在周一 12:00 pm - 1:00 pm Pacific Time。

Alliances 团队负责管理新 partner 的请求，并审查有关信息。他们将与您联系，提供有关申请状态的最新信息或要求您提供更多信息。如果您对请求的状态有任何疑问，请联系 [Alliances 团队](Alliance@gitlab.com)。

#### Step 2：创建技术文档、信息传递、识别客户等

完成了 Step 1，就可以进行 Step 2 的工作了!

1. 需要在您的网站上提供集成相关的技术文件。
2. 创建以集成的价值为重点的信息传递。
3. 确定共同的客户。
4. 按照[指南](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-guidelines/#brand-guidelines)在您的网站上添加 [GitLab Logo](https://about.gitlab.com/press/press-kit/#logos)。
5. 根据 [Integrate with GitLab](https://about.gitlab.com/handbook/alliances/integration-instructions/) 要求，添加您公司 Logo 和文档，以便在 GitLab Technology Partners 页面上展示及推广。

在 Merge Request 创建后，Alliances 团队会收到通知，并审核有关信息。如果符合要求，并且已经做好上线准备，Alliances 团队将批准 MR，在 GitLab 网站上线您的应用。

#### More

更多内容见 [GitLab Technology Partnerships](https://about.gitlab.com/partners/technology-partners/integrate/)。 -->

## FAQ

- Q: 如何开始与 GitLab 集成？

    A: 根据前文所述选择贡献代码或者是基于 API、Webhook、CI Template 来进行开发，可以参考其他与 GItLab 集成的应用。

- Q: 如何参与贡献？

    A: 目前我们的社区已有 4000 多名贡献者。想要参与贡献，可以访问[社区页面](https://about.gitlab.com/community/)了解更多资源、项目和活动信息。同时我们鼓励合作伙伴参与 GitLab 社区，贡献代码、参与或举办各种社区活动等，感兴趣的合作伙伴可以与我们联系。

- Q: 如何获得测试用许可证？

    A:我们为正在开发和测试 GitLab 集成的合作伙伴颁发旗舰版许可证。这些许可证仅对那些从事 GitLab 特定集成工作的人开放，有效期为 6 个月，根据要求最多可颁发给 10 个用户。
