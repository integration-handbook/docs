# 常用 GitLab API Client

与 GitLab 集成，除了使用 GitLab API，还有很多 GitLab 的技术合作伙伴，使用各种编程语言将 GitLab API 封装成 API Client。这使得无论用户使用哪种编程语言，都可以通过 import 该种语言的 API Client 轻松将 GitLab 引入自己的项目中，快速完成功能的调用与集成。

本文旨在帮助希望与 GitLab 集成的合作伙伴，快速找到他们正在使用编程语言的 API Client。

## Go

Golang 的 GitLab API Client，功能覆盖了大部分现有的 Gitlab API 调用。

地址：https://github.com/xanzy/go-gitlab

### 示例

```go
package main

import (
	"log"

	"github.com/xanzy/go-gitlab"
)

func main() {
	git, err := gitlab.NewClient("yourtokengoeshere")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Create new project
	p := &gitlab.CreateProjectOptions{
		Name:                 gitlab.String("My Project"),
		Description:          gitlab.String("Just a test project to play with"),
		MergeRequestsEnabled: gitlab.Bool(true),
		SnippetsEnabled:      gitlab.Bool(true),
		Visibility:           gitlab.Visibility(gitlab.PublicVisibility),
	}
	project, _, err := git.Projects.CreateProject(p)
	if err != nil {
		log.Fatal(err)
	}

	// Add a new snippet
	s := &gitlab.CreateProjectSnippetOptions{
		Title:           gitlab.String("Dummy Snippet"),
		FileName:        gitlab.String("snippet.go"),
		Content:         gitlab.String("package main...."),
		Visibility:      gitlab.Visibility(gitlab.PublicVisibility),
	}
	_, _, err = git.ProjectSnippets.CreateSnippet(project.ID, s)
	if err != nil {
		log.Fatal(err)
	}
}
```

## Ruby

GitLab API 的 Ruby wrapper 和 CLI，只支持 GitLab API v4。

- 地址：https://github.com/NARKOZ/
- 文档：https://www.rubydoc.info/gems/gitlab/frames
- 网站：https://narkoz.github.io/gitlab

### 安装

使用 rubygems

```shell
gem install gitlab
```
或将其添加到 Gemfile

```ruby
gem 'gitlab'
# gem 'gitlab', github: 'NARKOZ/gitlab'
```

### 配置示例

```ruby
Gitlab.configure do |config|
  config.endpoint       = 'https://example.net/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT'] and falls back to ENV['CI_API_V4_URL']
  config.private_token  = 'qEsq1pt6HJPaNciie3MG'       # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
  # Optional
  # config.user_agent   = 'Custom User Agent'          # user agent, default: 'Gitlab Ruby Gem [version]'
  # config.sudo         = 'user'                       # username for sudo mode, default: nil
end
```

## Python

`python-gitlab` 是一个 Python 包，提供了 Python 语言的 GitLab API Client 和一个 CLI 工具，只支持 GitLab API v4。

- 地址：https://github.com/python-gitlab/python-gitlab
- 文档：http://python-gitlab.readthedocs.org/en/stable/

### 安装

```shell
pip install python-gitlab
```

### 示例

```python
import gitlab

# private token or personal token authentication
gl = gitlab.Gitlab('http://10.0.0.1', private_token='JVNSESs8EwWRx5yDxM5q')

# oauth token authentication
gl = gitlab.Gitlab('http://10.0.0.1', oauth_token='my_long_token_here')

# job token authentication (to be used in CI)
import os
gl = gitlab.Gitlab('http://10.0.0.1', job_token=os.environ['CI_JOB_TOKEN'])

# anonymous gitlab instance, read-only for public resources
gl = gitlab.Gitlab('http://10.0.0.1')

# Define your own custom user agent for requests
gl = gitlab.Gitlab('http://10.0.0.1', user_agent='my-package/1.0.0')

# make an API request to create the gl.user object. This is mandatory if you
# use the username/password authentication.
gl.auth()
```

## Java

GitLab4J™ API (gitlab4j-api) 提供了一个功能齐全且易于使用的 Java 库，还提供了对使用 GitLab webhooks 和 system hooks 的全面支持。

- 地址：https://github.com/gitlab4j/gitlab4j-api
- 文档：https://javadoc.io/doc/org.gitlab4j/gitlab4j-api

### 配置

要求 Java 8 以上版本。

**Gradle: build.gradle**

```json
dependencies {
    ...
    compile group: 'org.gitlab4j', name: 'gitlab4j-api', version: '4.17.0'
}
```

**Maven: pom.xml**

```xml
<dependency>
    <groupId>org.gitlab4j</groupId>
    <artifactId>gitlab4j-api</artifactId>
    <version>4.17.0</version>
</dependency>
```

### 示例

```java
// Create a GitLabApi instance to communicate with your GitLab server
GitLabApi gitLabApi = new GitLabApi("http://your.gitlab.server.com", "YOUR_PERSONAL_ACCESS_TOKEN");

// Get the list of projects your account has access to
List<Project> projects = gitLabApi.getProjectApi().getProjects();
```

## 更多

更多信息见 [GitLab Technology Partners](https://about.gitlab.com/partners/technology-partners/#api-clients)。
