# 目录

## 前言

- [介绍](README.md)

## 集成指南

- [快速开始](integration-guides/README.md)
- [集成 Jira 开发环境搭建](integration-guides/jira_connect.md)
- [集成安全产品流程](integration-guides/secure_partner_integration.md)
   - [集成安全扫描器](integration-guides/security_scanner.md)
   - [SAST 扫描示例](integration-guides/sast-demo.md)

## 实用工具

- [API Client](tools/api-clients.md)
- [API 文档](https://docs.gitlab.com/ee/api/)
- [Webhook 文档](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)
- [CI/CD templates 开发者文档](https://docs.gitlab.com/ee/development/cicd/templates.html#development-guide-for-gitlab-cicd-templates)

## 集成示例

- [腾讯云 SCF 云函数集成](demo/tencent-serverless-scf.md)
