# GitLab CI 集成 SCF 云函数部署

## 编写业务代码

参考 [SCF Web 函数产品文档](https://cloud.tencent.com/document/product/583/56114)，编写您的业务函数代码和启动文件，此处以 Express 框架的 HelloWorld 为例：
函数代码 index.js：

```js
const express = require('express')
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get(`/`, (req, res) => {
  res.send({
    message: 'Hello Serverless'
  })
})

// Web 类型云函数，只能监听地址 0.0.0.0 和 端口 9000
app.listen(9000, '0.0.0.0', () => {
  console.log(`Server start on http://0.0.0.0:9000`)
})

```

启动文件 `scf_bootstrap`：

```shell
#!/bin/bash
/var/lang/node12/bin/node index.js
```

## 配置 `serverless.yml` 配置文件

参考腾讯云官网文档，配置 `serverless.yml`，示例文件如下：

```yml
component: scf
name: http

inputs:
  src:
    src: ./
    exclude:
      - .env
  # 指定 SCF 类型为 Web 类型
  type: web
  name: web-function
  region: ap-guangzhou
  handler: scf_bootstrap
  runtime: Nodejs12.16
  memorySize: 512 # 内存大小，单位MB
  installDependency: true
  events:
    - apigw:
        parameters:
          protocols:
            - http
            - https
          environment: release
          endpoints:
            - path: /
              method: ANY
              function:
                # 指定 API 类型为 Web 类型
                type: web
```

## 编写 `.gitlab-ci.yml`

```yml
image: node:latest

stages:  
   - deploy

production:  
  stage: deploy  
  before_script:    
     - npm config set prefix /usr/local    
     - npm install -g serverless    
     - npm install   
  script:    
     - serverless deploy
```

## 配置腾讯云账号信息

在 GitLab 控制台 **设置** -> **CI/CD** -> **变量** 中添加腾讯云账号信息，账号信息请在[腾讯云控制台](https://console.cloud.tencent.com/cam/capi)获取

```sh
SERVERLESS_PLATFORM_VENDOR: tencent #serverless 境外默认为 aws，配置为腾讯
TENCENT_SECRET_ID: #您的腾讯云账号 sercret ID
TENCENT_SECRET_KEY: #您的腾讯云账号 sercret key 
```
![](https://main.qcloudimg.com/raw/0c596ec7f1e95e905b44781a2206226a.png)


## 运行 CI 流水线

![](https://main.qcloudimg.com/raw/b131a10f852caa81301dfcd411dffe0d.png)


## 部署完成，测试结果

![](https://main.qcloudimg.com/raw/1f898e0c17ac6670d51ca818aa4c11f9.png)

访问 url

![](https://main.qcloudimg.com/raw/9001c8be3431ec95e67c88faa45a06b8.png)

## 更多

完整示例代码：https://gitlab.cn/tencentserverless/scf-cidemo

